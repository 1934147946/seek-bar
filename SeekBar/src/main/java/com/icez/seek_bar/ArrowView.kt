package com.icez.seek_bar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View

class ArrowView @JvmOverloads constructor(
    context: Context, val attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    private var mPath: Path?= null

    private var mPaint: Paint?= null

    private var mArrowBgColor:Int = Color.GRAY

    init {
        initStyleValue()
        mPath = Path()
        mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        mPaint?.strokeWidth = 1f
        mPaint?.color = mArrowBgColor
    }

    private fun initStyleValue(){
        val typedArray = context.obtainStyledAttributes(attrs,R.styleable.ArrowView)
        mArrowBgColor = typedArray.getColor(R.styleable.ArrowView_arrowBgColor,Color.GRAY)
        typedArray.recycle()
    }

    override fun onDraw(canvas: Canvas?) {
        mPath?.moveTo(0f,0f)
        mPath?.lineTo(width.toFloat(),0f)
        mPath?.lineTo(width/2f,height.toFloat())
        mPath?.let { mPaint?.let { it1 -> canvas?.drawPath(it, it1) } }
    }
}