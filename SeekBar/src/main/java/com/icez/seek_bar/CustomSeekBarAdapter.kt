package com.icez.seek_bar

import android.view.View

abstract class CustomSeekBarAdapter {
    // 获取提示布局
    abstract fun getHintViewResId():Int

    // 获取指示箭头的高度或者多余的高度，返回的是DP，不是px
    abstract fun getArrowHeight():Float

    // 获取提示文本字符串
    abstract fun getHintText(progress:Int):String?
}