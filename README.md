# SeekBar

#### 介绍
拖动条，用来替换系统自带的seekbar

#### 引入依赖

在项目的build.gradle配置

```groovy
allprojects {
		repositories {
			...
			maven { url 'https://www.jitpack.io' }
		}
	}
```

在app的build.gradle配置

```groovy
dependencies {
	        implementation 'com.gitee.1934147946:seek-bar:{version}'
	}
```

#### ui效果
使用的模拟器演示

![](https://images-1258389786.cos.ap-nanjing.myqcloud.com/GIF202343110817.gif)

