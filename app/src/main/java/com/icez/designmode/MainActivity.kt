package com.icez.designmode

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.icez.seek_bar.CustomSeekBar
import com.icez.seek_bar.CustomSeekBarAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val customSeekBar = findViewById<CustomSeekBar>(R.id.customSeekBar)


        customSeekBar.setAdapter(object: CustomSeekBarAdapter() {
            override fun getHintViewResId(): Int {
                return R.layout.seek_hint_bg
            }

            override fun getArrowHeight(): Float {
                return 4f
            }

            override fun getHintText(progress: Int): String? {
                return "p"+progress
            }


        })

    }
}